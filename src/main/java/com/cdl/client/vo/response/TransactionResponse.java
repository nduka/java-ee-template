/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client.vo.response;

/**
 *
 * @author abdulrasheedsoladoye
 */
public class TransactionResponse extends ServiceResponse
{
    
    public TransactionResponse (String code, String message, String description)
    {
        super (code, message, description);
    }
    
}
