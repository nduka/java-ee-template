/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client.vo.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Date;
import java.util.List;

/**
 *
 * @author abdulrasheedsoladoye
 */
public class ChannelResponse extends ServiceResponse
{

    private List<Data> data;

    public ChannelResponse (String code, String message, String description)
    {
        super (code, message, description);
    }

    public List<Data> getData ()
    {
        return data;
    }

    public void setData (List<Data> data)
    {
        this.data = data;
    }

    public class Data
    {

        private Long id;
        @JsonProperty("channel_code")
        private String code;
        private String name;
        private String description;
        @JsonProperty("is_enabled")
        private boolean enabled;
        @JsonProperty("created_at")
        private Date created;
        @JsonProperty("updated_at")
        private Date updated;

        public String getCode ()
        {
            return code;
        }

        public void setCode (String code)
        {
            this.code = code;
        }

        public String getName ()
        {
            return name;
        }

        public void setName (String name)
        {
            this.name = name;
        }

        public String getDescription ()
        {
            return description;
        }

        public void setDescription (String description)
        {
            this.description = description;
        }

        public boolean isEnabled ()
        {
            return enabled;
        }

        public void setEnabled (boolean enabled)
        {
            this.enabled = enabled;
        }

        public Date getCreated ()
        {
            return created;
        }

        public void setCreated (Date created)
        {
            this.created = created;
        }

        public Date getUpdated ()
        {
            return updated;
        }

        public void setUpdated (Date updated)
        {
            this.updated = updated;
        }

        public Long getId ()
        {
            return id;
        }

        public void setId (Long id)
        {
            this.id = id;
        }

        

    }
}
