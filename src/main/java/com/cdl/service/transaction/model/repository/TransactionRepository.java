/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.service.transaction.model.repository;

import com.cdl.service.transaction.model.Channel;
import com.cdl.service.transaction.model.Transaction;
import java.util.List;

/**
 *
 * @author abdulrasheedsoladoye
 */
public interface TransactionRepository
{

    void add (Channel channel);

    void update (Channel channel);

    Transaction getTransactions (Long channelId);

    List<Transaction> getTransactions (String userId, String userType, String agentId, 
            String channelId, String transactionStatusId, String transactionTypeId, 
            String paymnentMethod, String transactionAmount, String amount, String debitedWallet,
            Integer limit, Integer page, Integer from, Integer to);
}
