/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author abdulrasheedsoladoye
 */
@ApplicationPath("/")
public class BaseApplication extends Application
{

    private final Set<Object> singletons = new HashSet<> ();

    @Override
    public Set<Class<?>> getClasses ()
    {
        return Collections.EMPTY_SET;
    }

    @Override
    public Set<Object> getSingletons ()
    {
        return singletons;
    }
}
