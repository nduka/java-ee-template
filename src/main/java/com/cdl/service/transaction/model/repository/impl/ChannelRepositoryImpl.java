/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.service.transaction.model.repository.impl;

import com.cdl.service.transaction.model.Channel;
import com.cdl.service.transaction.model.repository.ChannelRepository;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Stateless
public class ChannelRepositoryImpl implements ChannelRepository
{

    @PersistenceContext(unitName = "TransactionPU")
    private EntityManager em;

    @Override
    public void add (Channel channel)
    {
        persist (channel);
    }

    @Override
    public void update (Channel channel)
    {
        merge (channel);
    }

    @Override
    public List<Channel> getChannel (Long channelId)
    {
        List<Channel> channelList = em.createNamedQuery ("Channel.findById", Channel.class)
                .setParameter ("id", channelId)
                .getResultList ();
        return channelList.isEmpty () ? null : channelList;
    }

    @Override
    public List<Channel> getChannel (String channelCode)
    {
        List<Channel> channelList = em.createNamedQuery ("Channel.findByCode", Channel.class)
                .setParameter ("code", channelCode)
                .getResultList ();
        return channelList.isEmpty () ? null : channelList;
    }

    private void persist (Channel channel)
    {
        em.persist (channel);
    }

    private void merge (Channel channel)
    {
        em.merge (channel);
    }

}
