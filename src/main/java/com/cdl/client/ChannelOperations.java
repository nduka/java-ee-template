/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client;

import com.cdl.client.services.ChannelService;
import com.cdl.client.vo.response.ServiceResponse;
import com.cdl.client.vo.response.ServiceStatusCodes;
import com.cdl.client.vo.request.ChannelRequest;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Path("v1/channels")
public class ChannelOperations
{

    @Resource
    private ManagedExecutorService executor;

    @Inject
    private ChannelService channelService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public void getChannel (@Suspended final AsyncResponse asyncResponse)
    {

        asyncResponse.setTimeout (0, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler ((AsyncResponse response)
                ->
        {
            ServiceResponse serviceResponse = new ServiceResponse (ServiceStatusCodes.VALIDATION_ERROR, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
            response.resume (serviceResponse);
        });

        executor.submit (()
                ->
        {
            try
            {
                asyncResponse.resume (channelService.getChannel (null));
            } catch (Exception e)
            {
                asyncResponse.resume (false);
            }
        });

    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getChannel (@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long channelId)
    {

        asyncResponse.setTimeout (0, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler ((AsyncResponse response)
                ->
        {
            ServiceResponse serviceResponse = new ServiceResponse (ServiceStatusCodes.VALIDATION_ERROR, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
            response.resume (serviceResponse);
        });

        executor.submit (()
                ->
        {
            try
            {
                asyncResponse.resume (channelService.getChannel (channelId));
            } catch (Exception e)
            {
                asyncResponse.resume (false);
            }
        });

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void createChannel (@Suspended final AsyncResponse asyncResponse, ChannelRequest request)
    {
        ServiceResponse serviceResponse = new ServiceResponse (ServiceStatusCodes.VALIDATION_ERROR, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
        asyncResponse.setTimeout (0, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler ((AsyncResponse response)
                ->
        {
            response.resume (serviceResponse);
        });

        executor.submit (()
                ->
        {
            try
            {
                asyncResponse.resume (channelService.createChannel (request.getCode (), request.getName (),
                        request.getDescription (), request.getEnabled ()));
            } catch (Exception e)
            {
                asyncResponse.resume (serviceResponse);
            }
        });
    }

    @PUT
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void updateChannel (@Suspended final AsyncResponse asyncResponse, 
            ChannelRequest request, @PathParam("id") Long channelId)
    {
        ServiceResponse serviceResponse = new ServiceResponse (ServiceStatusCodes.VALIDATION_ERROR, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
        asyncResponse.setTimeout (0, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler ((AsyncResponse response)
                ->
        {
            response.resume (serviceResponse);
        });

        executor.submit (()
                ->
        {
            try
            {
                asyncResponse.resume (channelService.updateChannel (channelId, request.getCode (), request.getName (),
                        request.getDescription (), request.getEnabled ()));
            } catch (Exception e)
            {
                asyncResponse.resume (serviceResponse);
            }
        });
    }
    
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public void deleteChannel (@Suspended final AsyncResponse asyncResponse, @PathParam("id") Long channelId)
    {
        ServiceResponse serviceResponse = new ServiceResponse (ServiceStatusCodes.VALIDATION_ERROR, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
        asyncResponse.setTimeout (0, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler ((AsyncResponse response)
                ->
        {
            response.resume (serviceResponse);
        });

        executor.submit (()
                ->
        {
            try
            {
                asyncResponse.resume (channelService.deleteChannel (channelId));
            } catch (Exception e)
            {
                asyncResponse.resume (serviceResponse);
            }
        });
    }
}
