/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client.services;

import com.cdl.client.vo.response.ServiceStatusCodes;
import com.cdl.client.vo.response.TransactionResponse;
import com.cdl.service.transaction.model.Transaction;
import com.cdl.service.transaction.model.repository.TransactionRepository;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Stateless
public class TransactionServices
{

    @Inject
    TransactionRepository transactionRepository;

    public TransactionResponse getTransactions (String userId, String userType,
            String agentId, String channelId, String transactionStatusId,
            String transactionTypeId, String paymnentMethod,
            String transactionAmount, String amount, String debitedWallet, Integer limit,
            Integer page, Integer from, Integer to)
    {
        TransactionResponse response = new TransactionResponse (ServiceStatusCodes.NOT_FOUND, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND));

        List<Transaction> transactionList = transactionRepository.getTransactions (userId, userType, agentId, channelId,
                transactionStatusId, transactionTypeId, paymnentMethod,
                transactionAmount, amount, debitedWallet, limit, page, from, to);
        return response;
    }

}
