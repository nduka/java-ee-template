/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client.services;

import com.cdl.client.vo.response.ChannelResponse;
import com.cdl.client.vo.response.ServiceResponse;
import com.cdl.client.vo.response.ServiceStatusCodes;
import com.cdl.service.transaction.model.Channel;
import com.cdl.service.transaction.model.repository.ChannelRepository;
import com.cdl.util.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Stateless
public class ChannelService
{

    @Inject
    ChannelRepository channelRepository;

    public ChannelResponse getChannel (Long channelId)
    {
        ChannelResponse response = new ChannelResponse (ServiceStatusCodes.NOT_FOUND, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND));

        List<ChannelResponse.Data> channelList = new ArrayList<> ();

        List<Channel> channels = channelRepository.getChannel (channelId);
        if (channels != null)
        {
            response.setCode (ServiceStatusCodes.SUCCESS);
            response.setMessage (ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.SUCCESS));
            response.setDescription (response.getMessage ());

            channels.forEach ((c) ->
            {
                ChannelResponse.Data data = response.new Data ();
                data.setId (c.getId ());
                data.setCode (c.getCode ());
                data.setDescription (c.getDescription ());
                data.setName (c.getName ());
                data.setEnabled (c.isEnabled ());

                data.setCreated (c.getDateCreated ());
                data.setUpdated (c.getDateUpdated ());
                channelList.add (data);
            });
        }

        response.setData (channelList);

        return response;
    }

    public ChannelResponse createChannel (String code, String name, String description, boolean enabled)
    {
        ChannelResponse response = new ChannelResponse (ServiceStatusCodes.NOT_FOUND, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND));

        List<Channel> channelList = channelRepository.getChannel (code);
        if (channelList == null)
        {
            Date currentTime = Utils.getCurrentTime ();

            Channel channel = new Channel ();
            channel.setCode (code);
            channel.setName (name);
            channel.setDescription (description);
            channel.setEnabled (enabled);
            channel.setDateCreated (currentTime);
            channel.setDateUpdated (currentTime);

            channelRepository.add (channel);

            ChannelResponse.Data data = response.new Data ();
            data.setId (channel.getId ());
            data.setCode (code);
            data.setName (name);
            data.setDescription (description);
            data.setEnabled (enabled);
            data.setCreated (currentTime);
            data.setUpdated (currentTime);

            response.setData (new ArrayList<ChannelResponse.Data> ()
            {
                {
                    add (data);
                }
            });
            response.setCode (ServiceStatusCodes.SUCCESS);
            response.setMessage (ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.SUCCESS));
            response.setDescription (response.getMessage ());
        } else
        {
            response.setCode (ServiceStatusCodes.VALIDATION_ERROR);
            response.setMessage (ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
            response.setDescription (response.getMessage ());
        }

        return response;
    }

    public ChannelResponse updateChannel (Long channelId, String code, String name,
            String description, boolean enabled)
    {

        ChannelResponse response = new ChannelResponse (ServiceStatusCodes.NOT_FOUND, 
                ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND), 
                ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND));

        List<Channel> channelList = channelRepository.getChannel (channelId);
        if (channelList != null)
        {
            Channel channel = channelList.get (0);
            channel.setName (name);
            channel.setDescription (description);
            channel.setCode (code);
            channel.setEnabled (enabled);
            channel.setDateUpdated (Utils.getCurrentTime ());

            channelRepository.update (channel);

            response.setCode (ServiceStatusCodes.SUCCESS);
            response.setMessage (ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.SUCCESS));
            response.setDescription (response.getMessage ());

            ChannelResponse.Data data = response.new Data ();
            data.setId (channel.getId ());
            data.setCode (code);
            data.setName (name);
            data.setDescription (description);
            data.setEnabled (enabled);
            data.setCreated (channel.getDateCreated ());
            data.setUpdated (channel.getDateUpdated ());

            response.setData (new ArrayList<ChannelResponse.Data> ()
            {
                {
                    add (data);
                }
            });
        }
        return response;
    }

    public ServiceResponse deleteChannel (Long channelId)
    {
        ServiceResponse response = new ServiceResponse (ServiceStatusCodes.NOT_FOUND, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.NOT_FOUND));

        List<Channel> channelList = channelRepository.getChannel (channelId);
        if (channelList != null)
        {
            Channel channel = channelList.get (0);
            channel.setDateDeleted (Utils.getCurrentTime ());
            channel.setDateUpdated (Utils.getCurrentTime ());

            channelRepository.update (channel);

            response.setCode (ServiceStatusCodes.SUCCESS);
            response.setMessage (ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.SUCCESS));
            response.setDescription (response.getMessage ());

        }
        return response;
    }
}
