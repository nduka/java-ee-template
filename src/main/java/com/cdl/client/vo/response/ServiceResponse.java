/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client.vo.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author abdulrasheedsoladoye
 */
public class ServiceResponse
{

    @JsonProperty("resp_code")
    private String code;
    @JsonProperty("resp_message")
    private String message;
    @JsonProperty("resp_description")
    private String description;

    public ServiceResponse (String code, String message, String description)
    {
        this.code = code;
        this.message = message;
        this.description = description;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

}
