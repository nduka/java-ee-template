/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.service.transaction.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Entity
@Table(name = "transaction_statuses")
public class TransactionStatus implements Serializable
{

    @OneToMany(mappedBy = "transactionStatus")
    private List<Transaction> transactions;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String description;
    @Column(name = "deleted_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateDeleted;
    @Column(name = "created_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "updated_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateUpdated;

    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public Date getDateDeleted ()
    {
        return dateDeleted;
    }

    public void setDateDeleted (Date dateDeleted)
    {
        this.dateDeleted = dateDeleted;
    }

    public Date getDateCreated ()
    {
        return dateCreated;
    }

    public void setDateCreated (Date dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated ()
    {
        return dateUpdated;
    }

    public void setDateUpdated (Date dateUpdated)
    {
        this.dateUpdated = dateUpdated;
    }

    public List<Transaction> getTransactions ()
    {
        return transactions;
    }

    public void setTransactions (List<Transaction> transactions)
    {
        this.transactions = transactions;
    }

    
    
}
