/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.service.transaction.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Entity
@Table(name = "channels")
@NamedQueries(
        {
            @NamedQuery(name = "Channel.findById", query = "SELECT c FROM Channel c WHERE (NULL = :id OR c.id = :id) AND c.dateDeleted = NULL"),
            @NamedQuery(name = "Channel.findByCode", query = "SELECT c FROM Channel c WHERE (NULL = :code OR c.code = :code) AND c.dateDeleted = NULL"),
            @NamedQuery(name = "Channel.ListAll", query = "SELECT c FROM Channel c WHERE (NULL = :enabled OR c.enabled = :enabled) AND c.dateDeleted = NULL")
        })
public class Channel implements Serializable
{

    @OneToMany(mappedBy = "channel")
    private List<Transaction> transactions;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "channel_code")
    private String code;
    private String name;
    private String description;
    @Column(name = "is_enabled")
    private boolean enabled;
    @Column(name = "created_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "updated_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateUpdated;
    @Column(name = "deleted_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateDeleted;

    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public String getCode ()
    {
        return code;
    }

    public void setCode (String code)
    {
        this.code = code;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public Date getDateCreated ()
    {
        return dateCreated;
    }

    public void setDateCreated (Date dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated ()
    {
        return dateUpdated;
    }

    public void setDateUpdated (Date dateUpdated)
    {
        this.dateUpdated = dateUpdated;
    }

    public Date getDateDeleted ()
    {
        return dateDeleted;
    }

    public void setDateDeleted (Date dateDeleted)
    {
        this.dateDeleted = dateDeleted;
    }

    public boolean isEnabled ()
    {
        return enabled;
    }

    public void setEnabled (boolean enabled)
    {
        this.enabled = enabled;
    }

    public List<Transaction> getTransactions ()
    {
        return transactions;
    }

    public void setTransactions (List<Transaction> transactions)
    {
        this.transactions = transactions;
    }

}
