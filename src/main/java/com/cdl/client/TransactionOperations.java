/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client;

import com.cdl.client.services.TransactionServices;
import com.cdl.client.vo.response.ServiceResponse;
import com.cdl.client.vo.response.ServiceStatusCodes;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Path("v1/transactions")
public class TransactionOperations
{

    @Resource
    private ManagedExecutorService executor;

    @Inject
    private TransactionServices transactionServices;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public void getTransactions (@Suspended final AsyncResponse asyncResponse,
            @HeaderParam("userId") String userId, @HeaderParam("userType") String userType,
            @QueryParam("agent_id") String agentId, @QueryParam("channel_id") String channelId,
            @QueryParam("transaction_status_id") String transactionStatusId,
            @QueryParam("transaction_type_id") String transactionTypeId,
            @QueryParam("payment_method") String paymnentMethod,
            @QueryParam("transaction_amount") String transactionAmount, @QueryParam("amount") String amount,
            @QueryParam("debited_wallet") String debitedWallet,
            @QueryParam("per_page") Integer perPage,
            @QueryParam("pageSize") Integer pageSize,
            @QueryParam("page") Integer page, @QueryParam("from") Integer from, @QueryParam("to") Integer to)
    {
        int limit = pageSize == null ? (perPage != null ? perPage : 100) : 100;

        asyncResponse.setTimeout (0, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler ((AsyncResponse response)
                ->
        {
            ServiceResponse serviceResponse
                    = new ServiceResponse (ServiceStatusCodes.VALIDATION_ERROR,
                            ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR),
                            ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
            response.resume (serviceResponse);
        });

        executor.submit (()
                ->
        {
            try
            {
                asyncResponse.resume (transactionServices
                        .getTransactions (userId, userType, agentId, channelId,
                                transactionStatusId, transactionTypeId, paymnentMethod,
                                transactionAmount, amount, debitedWallet, limit, page, from, to));
            } catch (Exception e)
            {
                asyncResponse.resume (false);
            }
        });

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public void saveTransactions (@Suspended final AsyncResponse asyncResponse)
    {

        asyncResponse.setTimeout (0, TimeUnit.SECONDS);
        asyncResponse.setTimeoutHandler ((AsyncResponse response)
                ->
        {
            ServiceResponse serviceResponse = new ServiceResponse (ServiceStatusCodes.VALIDATION_ERROR, ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR), ServiceStatusCodes.MESSAGES.get (ServiceStatusCodes.VALIDATION_ERROR));
            response.resume (serviceResponse);
        });

        executor.submit (()
                ->
        {
            try
            {
//                asyncResponse.resume (transactionServices.getTransactions ());
            } catch (Exception e)
            {
                asyncResponse.resume (false);
            }
        });

    }
}
