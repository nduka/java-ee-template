/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.util;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author abdulrasheedsoladoye
 */
public class Utils
{

    public static Date getCurrentTime ()
    {
        Calendar calendar = getCalender ();
        calendar.setTime (new Date ());
        return calendar.getTime ();
    }

    private static Calendar getCalender ()
    {
        Calendar calendar = Calendar.getInstance ();
        calendar.setTimeZone (TimeZone.getTimeZone ("Africa/Lagos"));
        return calendar;
    }
}
