/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.client.vo.response;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author abdulrasheedsoladoye
 */
public class ServiceStatusCodes
{
//"NOT_FOUND" => '01',
//        "INVALID_API_KEY" => '02',
//        "VALIDATION_ERROR" => '03'

    public static final String SUCCESS = "00";
    public static final String NOT_FOUND = "01";
    public static final String INVALID_API_KEY = "02";
    public static final String VALIDATION_ERROR = "03";
    
    public static final Map<String, String> MESSAGES = new HashMap<String, String> ()
    {
        {
            put (SUCCESS, "Approved or completed successfully");
            put (NOT_FOUND, "Record not found");
            put (INVALID_API_KEY, "Invalid API Keys");
            put (VALIDATION_ERROR, "Validation Error");
        }
    };
}
