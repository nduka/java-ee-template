/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.service.transaction.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author abdulrasheedsoladoye
 */


//SELECT * FROM transactions t where t.agent_id = '49f7ea00-f6fa-4600-6df8-08d7d684ac05' AND 
// t.channel_id = 1 AND t.transaction_service_id = 'TS2'
//AND t.transaction_type_id = 2 AND t.transaction_status_id = 1 AND 
// t.transaction_ref = '00023423e11749789g99311669660021589119082' AND 
//t.transaction_amount  = 50000.00 AND t.payment_method = 0 AND t.debited_wallet is NULL 
//ORDER  BY t.created_at DESC;


@Entity
@Table(name = "transactions")
@NamedQueries({
@NamedQuery(name = "Transaction.filter",query = "SELECT t FROM Transaction t "
        + "WHERE (NULL = :agentId OR t.agentId = :agentId) AND (NULL = :channelId OR t.channel.id = :channelId) "
        + "AND (NULL = :serviceId OR t.transactionService.id = :serviceId) AND "
        + "(NULL = :transactionType OR t.transactionTypes.id = :transactionType) AND "
        + "(NULL = :status OR t.transactionStatus.id = :status) AND (NULL = :ref OR t.transactionRef = :ref) "
        + "AND (NULL = :amount OR t.amount = :amount) AND (NULL = :paymentMethod OR t.paymentMethod = :paymentMethod) "
        + "AND (NULL = :wallet OR t.debitedWallet = :wallet) AND (NULL = :from OR NULL = :to OR (t.dateCreated BETWEEN :from AND :to))"
        + "ORDER BY t.dateCreated DESC")
})
public class Transaction implements Serializable
{

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "request_id")
    private String requestId;
    @Column(name = "agent_id")
    private String agentId;
    @Column(name = "transaction_ref")
    private String transactionRef;
    @Column(name = "transaction_description")
    private String narration;
    @Column(name = "transaction_amount")
    private BigDecimal amount;
    @Column(name = "payment_method")
    private int paymentMethod;
    @Column(name = "transaction_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date transactionDate;
    @Column(name = "transaction_type_id")
    @ManyToOne
    private TransactionTypes transactionTypes;
    @Column(name = "transaction_status_id")
    @ManyToOne
    private TransactionStatus transactionStatus;
    @ManyToOne
    private Channel channel;
    private String longitude;
    private String latitude;
    @Column(name = "source_model")
    private String sourceModel;
    @Column(name = "source_model_id")
    private Long sourceModelId;
    @Column(name = "payment_status")
    private Integer paymentStatus;
    @Column(name = "original_trans_id")
    private Integer originalTransactionId;
    @Column(name = "value_given")
    private boolean valueGiven;
    @Column(name = "is_reversed")
    private boolean reversed;
    @Column(name = "deleted_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateDeleted;
    @Column(name = "created_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "updated_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateUpdated;
    @Column(name = "transaction_service_id")
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private TransactionService transactionService;
    @Column(name = "commission_settled")
    private boolean commissionSettled;
    @Column(name = "fee_settled")
    private boolean feeSettled;
    @Column(name = "extra_data")
    private String extraData;
    @Column(name = "rel_to_parent")
    private String relationShipToParent;
    @Column(name = "reason")
    private String reason;
    @Column(name = "agent_username")
    private String agentUsername;
    @Column(name = "original_amount")
    private BigDecimal originalAmount;
    @Column(name = "partner_type")
    private String partnerType;
    @Column(name = "user_obj",columnDefinition = "TEXT")
    private String userObject;
    @Column(name = "debited_wallet")
    private String debitedWallet;
    @Column(name = "stakeholders", columnDefinition = "TEXT")
    private String stakeholders;
    

    public Long getId ()
    {
        return id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public String getRequestId ()
    {
        return requestId;
    }

    public void setRequestId (String requestId)
    {
        this.requestId = requestId;
    }

    public String getAgentId ()
    {
        return agentId;
    }

    public void setAgentId (String agentId)
    {
        this.agentId = agentId;
    }

    public String getTransactionRef ()
    {
        return transactionRef;
    }

    public void setTransactionRef (String transactionRef)
    {
        this.transactionRef = transactionRef;
    }

    public String getNarration ()
    {
        return narration;
    }

    public void setNarration (String narration)
    {
        this.narration = narration;
    }

    public BigDecimal getAmount ()
    {
        return amount;
    }

    public void setAmount (BigDecimal amount)
    {
        this.amount = amount;
    }

    public int getPaymentMethod ()
    {
        return paymentMethod;
    }

    public void setPaymentMethod (int paymentMethod)
    {
        this.paymentMethod = paymentMethod;
    }

    public Date getTransactionDate ()
    {
        return transactionDate;
    }

    public void setTransactionDate (Date transactionDate)
    {
        this.transactionDate = transactionDate;
    }

    public TransactionTypes getTransactionTypes ()
    {
        return transactionTypes;
    }

    public void setTransactionTypes (TransactionTypes transactionTypes)
    {
        this.transactionTypes = transactionTypes;
    }

    public TransactionStatus getTransactionStatus ()
    {
        return transactionStatus;
    }

    public void setTransactionStatus (TransactionStatus transactionStatus)
    {
        this.transactionStatus = transactionStatus;
    }

    public Channel getChannel ()
    {
        return channel;
    }

    public void setChannel (Channel channel)
    {
        this.channel = channel;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getSourceModel ()
    {
        return sourceModel;
    }

    public void setSourceModel (String sourceModel)
    {
        this.sourceModel = sourceModel;
    }

    public Long getSourceModelId ()
    {
        return sourceModelId;
    }

    public void setSourceModelId (Long sourceModelId)
    {
        this.sourceModelId = sourceModelId;
    }

    public Integer getPaymentStatus ()
    {
        return paymentStatus;
    }

    public void setPaymentStatus (Integer paymentStatus)
    {
        this.paymentStatus = paymentStatus;
    }

    public Integer getOriginalTransactionId ()
    {
        return originalTransactionId;
    }

    public void setOriginalTransactionId (Integer originalTransactionId)
    {
        this.originalTransactionId = originalTransactionId;
    }

    public boolean isValueGiven ()
    {
        return valueGiven;
    }

    public void setValueGiven (boolean valueGiven)
    {
        this.valueGiven = valueGiven;
    }

    public boolean isReversed ()
    {
        return reversed;
    }

    public void setReversed (boolean reversed)
    {
        this.reversed = reversed;
    }

    public TransactionService getTransactionService ()
    {
        return transactionService;
    }

    public void setTransactionService (TransactionService transactionService)
    {
        this.transactionService = transactionService;
    }

    public boolean isCommissionSettled ()
    {
        return commissionSettled;
    }

    public void setCommissionSettled (boolean commissionSettled)
    {
        this.commissionSettled = commissionSettled;
    }

    public boolean isFeeSettled ()
    {
        return feeSettled;
    }

    public void setFeeSettled (boolean feeSettled)
    {
        this.feeSettled = feeSettled;
    }

    public String getExtraData ()
    {
        return extraData;
    }

    public void setExtraData (String extraData)
    {
        this.extraData = extraData;
    }

    public String getRelationShipToParent ()
    {
        return relationShipToParent;
    }

    public void setRelationShipToParent (String relationShipToParent)
    {
        this.relationShipToParent = relationShipToParent;
    }

    public String getReason ()
    {
        return reason;
    }

    public void setReason (String reason)
    {
        this.reason = reason;
    }

    public String getAgentUsername ()
    {
        return agentUsername;
    }

    public void setAgentUsername (String agentUsername)
    {
        this.agentUsername = agentUsername;
    }

    public BigDecimal getOriginalAmount ()
    {
        return originalAmount;
    }

    public void setOriginalAmount (BigDecimal originalAmount)
    {
        this.originalAmount = originalAmount;
    }

    public String getPartnerType ()
    {
        return partnerType;
    }

    public void setPartnerType (String partnerType)
    {
        this.partnerType = partnerType;
    }

    public String getUserObject ()
    {
        return userObject;
    }

    public void setUserObject (String userObject)
    {
        this.userObject = userObject;
    }

    public String getDebitedWallet ()
    {
        return debitedWallet;
    }

    public void setDebitedWallet (String debitedWallet)
    {
        this.debitedWallet = debitedWallet;
    }

    public String getStakeholders ()
    {
        return stakeholders;
    }

    public void setStakeholders (String stakeholders)
    {
        this.stakeholders = stakeholders;
    }

    public Date getDateDeleted ()
    {
        return dateDeleted;
    }

    public void setDateDeleted (Date dateDeleted)
    {
        this.dateDeleted = dateDeleted;
    }

    public Date getDateCreated ()
    {
        return dateCreated;
    }

    public void setDateCreated (Date dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated ()
    {
        return dateUpdated;
    }

    public void setDateUpdated (Date dateUpdated)
    {
        this.dateUpdated = dateUpdated;
    }

}
