
# CDL Wallet Service
---

## JDK Version
---
Minimum JDK version [1.8](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)

## Maven Setup
---
This project is a **maven** project.

Maven is a rather old and popular tool that can do almost everything. Among other things it can:

* compile java sources
* package the project into a jar/war file
* resolve and download dependencies

Maven is a convention-over-configuration tool. To get along with maven, the following requirements must be met:

* all sources are placed in the `src/main/java directory`
* all resources are placed in the `src/main/resources directory`
* all test sources are placed in the `src/test/java directory`
* all test resources are placed in the `src/test/resources` directory
each project will contain a pom.xml file in the project root directory (more on that later)

To install maven, download the binary zip from https://maven.apache.org/download.cgi and follow the install instructions at https://maven.apache.org/install.html. Linux users: just use your package manager.

When everything is set up correctly, then the following one-liners can be used on the command line (in the project root directory):

* `mvn compile` (compile all the code)
* `mvn test` (compile and run the tests)
* `mvn package` (package the entire project)
* `mvn clean` (delete the compiled code and packages)
* `mvn clean package` (delete all, compile, run tests, package)

After the `mvn package` command runs and completes, the artifact is stored in the `${Project Folder}/target/`

### Application Server
[WildFly](https://wildfly.org/), formerly known as JBoss AS (Application Server), or simply JBoss, is an application server authored by JBoss, now developed by Red Hat. 
WildFly is written in Java and implements the Java Platform, Enterprise Edition specification. 
 
### AS Version
The version of wildfly used for this service is [Version 19.1.0.Final](https://download.jboss.org/wildfly/19.1.0.Final/wildfly-19.1.0.Final.zip)

## Staring up server

* Change directory to `${WILDFLY_HOME}/bin`
* run the command `./standalone.sh -c standalone-full.xml`

For more information on Widfly directory structure and configurations see [click here](https://docs.wildfly.org/19.1/Getting_Started_Guide.html)

## Deployment
Copy `.war` file from maven target directory and drop in `${WILDFLY_HOME}/standalone/deployments`

### Deployment indicator

The following file will be see in the deployments directory

* `{app}.war.isdeploying` When deployment is in progress
* `{app}.war.deployed` When deployment is successful
* `{app}.war.failed` When deployment failed
* `{app}.war.undeployed` When application is undeployed

## Configuration
---

### Application Server Configuration
This services uses the `standalone-full.xml` which implements the Jakarta Full Platform certified configuration including all the required technologies.

### Data source Configuration
The following are datasource configuration parameters used

#### Transaction Module
* jndi-name `java:/transactionDS` 
* pool name `TransactionPU`

### Queue Configuration

#### Application variables 

