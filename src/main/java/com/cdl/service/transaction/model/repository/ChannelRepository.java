/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.service.transaction.model.repository;

import com.cdl.service.transaction.model.Channel;
import java.util.List;

/**
 *
 * @author abdulrasheedsoladoye
 */
public interface ChannelRepository
{

    void add (Channel channel);

    void update (Channel channel);

    List<Channel> getChannel (Long channelId);

    List<Channel> getChannel (String channelCode);

}
