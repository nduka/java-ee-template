/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdl.service.transaction.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author abdulrasheedsoladoye
 */
@Entity
@Table(name = "transaction_services")
public class TransactionService implements Serializable
{

    @OneToMany(mappedBy = "transactionService")
    private List<Transaction> transactions;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transaction_type_id;
    private String id;
    @Column(name = "service_name")
    private String serviceName;
    private String description;
    @Column(name = "created_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateCreated;
    @Column(name = "updated_at")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dateUpdated;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getServiceName ()
    {
        return serviceName;
    }

    public void setServiceName (String serviceName)
    {
        this.serviceName = serviceName;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public Long getTransaction_type_id ()
    {
        return transaction_type_id;
    }

    public void setTransaction_type_id (Long transaction_type_id)
    {
        this.transaction_type_id = transaction_type_id;
    }

    public Date getDateCreated ()
    {
        return dateCreated;
    }

    public void setDateCreated (Date dateCreated)
    {
        this.dateCreated = dateCreated;
    }

    public Date getDateUpdated ()
    {
        return dateUpdated;
    }

    public void setDateUpdated (Date dateUpdated)
    {
        this.dateUpdated = dateUpdated;
    }

    public List<Transaction> getTransactions ()
    {
        return transactions;
    }

    public void setTransactions (List<Transaction> transactions)
    {
        this.transactions = transactions;
    }

}
